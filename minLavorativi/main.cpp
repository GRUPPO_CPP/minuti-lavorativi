#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iterator>
#include <fstream>
#include <algorithm> 

using namespace std;

typedef struct {
    int ora;
    int min;
    string cod;
}attraversa;

void tuttiParametri(string file, string codice);
void vectorSTLPrint(vector<attraversa> dip, string cod);
void senzaCodice(string file);

int main(int argc, char* argv[]) {
    /*string file = argv[1];
    string codice = argv[2];*/
    string file = "passaggi.txt";
    string codice = "abc123";
    //string codice = "";

    //se ha tutti due parametri
    if (file != "" && codice != "") {
        tuttiParametri(file, codice);
    }
    //se ha solo il parametro file
    else if (file != "" && codice == "") {  
        senzaCodice(file);
    }
    else{
        cout << "\nil file non esiste\n";
    }

    return 0;
}

void tuttiParametri(string file, string codice) {
    int ora, min;
    string cod;
    attraversa att;
    vector<attraversa> dip;
    ifstream ReadFile;
    ReadFile.open(file);


    if (ReadFile.is_open()) {
        while (!ReadFile.eof()) {
            ReadFile >> ora;
            ReadFile >> min;
            ReadFile >> cod;

            att.ora = ora;
            att.min = min;
            att.cod = cod;

            //se il codide uguale al codice dal file aggiungo al vettore
            if (!ReadFile.eof() && codice == cod) {
                dip.push_back(att);
            }
        }

        ReadFile.close();
        vectorSTLPrint(dip, codice);
    }
    else {
        cout << "i dati che lei ha inseriti sono sbagliati! riprova con le dati giusti";
    }
}

void senzaCodice(string file) {
    vector<string> per;
    ifstream ReadFile;
    int ora, min;
    string cod;
    ReadFile.open(file);

    if (ReadFile.is_open()) {
        while (!ReadFile.eof()) {
            ReadFile >> ora;
            ReadFile >> min;
            ReadFile >> cod;

            //aggiungo le codice in un vettore
            if (!ReadFile.eof()) {
                per.push_back(cod);
            }
        }
        ReadFile.close();

        sort(per.begin(), per.end());
        auto it = unique(per.begin(), per.end());

        per.erase(it, per.end());

        cout << "ci sono " << per.size() << " dipendenti diversi";
    }
    else {
        cout << "i dati che lei ha inseriti sono sbagliati! riprova con le dati giusti";
    }
}

void vectorSTLPrint(vector<attraversa> dip, string cod) {
    int x = 0;
    int ent = 0 , usc = 0; 

    int prima = (dip.front().ora * 60) + dip.front().min;
    int ultima = (dip.back().ora * 60) + dip.back().min;

    for (int i = 0; i < dip.size(); i++){
        
        if (i % 2 == 0) {
            ent += ((dip.at(i).ora * 60) + dip.at(i).min);
        }
        else {
            usc += ((dip.at(i).ora * 60) + dip.at(i).min);
        }
    }

    cout << "\nIl dipendente " << cod << " ha lavorato per <" << usc - ent << " minuti>";
    cout << "\ne' ha fatto <" << (ultima - prima) - (usc - ent) << " minuti> di pause\n";
      
}
